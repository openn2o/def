# def 条件编译处理工具

#### 项目介绍
这是一个编程语言无关的条件编译插件。 首先你要确认的是你的代码需要使用条件编译，或者有多种模态发布到不同平台的限制。这个工具本质是和任何语言无关，你可以在你的代码里
免费使用。




#### 安装教程

下载以后解压到系统的/bin目录下，或者在PATH里指定路径。

#### 使用说明

### 1 宏注释

``` 
{% if  SCRIPT  == "lua" then %}
     return exports.GetValueByType (eax.value);
{% end %}

{% if  SCRIPT  == "neko" then %}
     return exports.GetValueByType (eax.value);
{% end %}


{% if  SCRIPT  == "c" then %}
     return exports.GetValueByType (eax.value);
{% end %}
```
使用方式

> lua def.lua 输入文件 输出文件 SCRIPT=lua  DEBUG=1

2 inline macro

``` haskell
{-inline_listense-}

Copyright (c) 2018 agent.zy@aliyun.com

{-inline_listense-}
```

在代码中内联该语法会将不同的inline macro 输出到指定位置

3 函数定义

``` handlebars
 {%   
    local lua_member_call    ="{{item}}:{{item1}}({{item2}})"
    local lua_string_add_val ="\"{{item}}\" .. {{item2}}"
    local lua_string_add_string ="\"{{item}}\" + \"{{item2}}\""
 %}
```

4 内联函数

``` javascript
{-inline_max-}
function  max (a, b) {
return a>b;
}
{-inline_max-}
```

如何使用我们定义的内联函数

> {*blocks.inline_max*}
> 
5 替换模式

``` clojure
{% 
local string_macro ="\"{{item}}\""
%}
{* MACRO(string_macro)(context) *}
{* MACRO(string_macro){ item = "string-macro-context" } *}
```

例子 ：


``` javascript
{-inline_esprima_parse-}
    var options = {
        attachComment: false,
        range: false,
        loc: false,
        sourceType: "script",
        tolerant: true
    };
            
    options.tokens = false;
    var result = exports.esprima.parse(buff, options);
    
{-inline_esprima_parse-}

/////
/////  生成code 
/////
{-inline_generate_code-}
 exports.lexerGenerateCode(result);
{-inline_generate_code-}


////
////   生成字节码
////

{-inline_generate_mid-}
 exports.lexerGenerateMidCode(result);
{-inline_generate_mid-}



{% if  SCRIPT  == "lua" then %}
exports.Main = function (buff) {
    {% if  DEBUG  then %}
        console.log("lua mode.")
    {% end %}
    ///生成ast解析
    {*blocks.inline_esprima_parse*} 
    ///生成code
    {*blocks.inline_generate_code*}       
}
{% end %}


{% if  SCRIPT  == "c" then %}
exports.Main = function (str) {
    {% if  DEBUG  then %}
        console.log("c mode.")
    {% end %}
    {*blocks.inline_esprima_parse*}
    {*blocks.inline_generate_code*} 
}
{% end %}
```




